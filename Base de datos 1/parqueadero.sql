create database parqueadero;
use parqueadero;
create table personas(
	doc_id varchar(10),
    nombre varchar(30),
    apellido varchar(30),
    telefono varchar(15),
    direccion varchar(40),
    fecha_nacimiento date,
    CONSTRAINT PK_Person primary key (doc_id)
)engine = InnoDB;

create table usuario(
	nick varchar(20),
    correo varchar(64),
    contraseña varchar(16),
    id_rol int,
    doc_id varchar(10),
    primary key (nick),
    FOREIGN KEY (id_rol) REFERENCES rol_usuario(id),
	FOREIGN KEY (doc_id) REFERENCES personas(doc_id)
)engine = InnoDB;

create table rol_usuario(
	id int not null auto_increment,
    nombre_rol varchar(15) unique,
    descripcion varchar (50),
    primary key (id)
)engine = InnoDB;

create table vehiculo(
	placa varchar(6),
    id_tipo_vehiculo int,
    nick varchar(15),
    caracteristicas varchar(100),
    primary key (placa),
    FOREIGN KEY (id_tipo_vehiculo) REFERENCES tipo_vehiculo(id),
    FOREIGN KEY (nick) REFERENCES usuario(nick)
)engine = InnoDB;

create table tipo_vehiculo(
	id int not null auto_increment,
    tipo varchar(10),
    descripcion varchar(50),
    primary key (id)
)engine = InnoDB;

create table plaza(
	id varchar (4) not null unique,
    tipo varchar(20),
    disponibilidad boolean default true,
    primary key(id)
)engine = InnoDB;

create table factura(
	id int not null auto_increment,
    fecha_ingreso datetime,
    fecha_salida datetime,
    id_tarifa int,
    placa_vehiculo varchar(6),
    id_plaza varchar(4),
    nick_vigilante varchar(20),
    primary key(id),
    foreign key(id_tarifa) references tarifa(id),
    foreign key(placa_vehiculo) references vehiculo(placa),
    foreign key(id_plaza) references plaza(id),
    foreign key(nick_vigilante) references usuario(nick)
)engine = InnoDB;

create table tarifa(
	id int not null auto_increment,
    valor double,
    tipo varchar(20),
    primary key(id)
)engine = InnoDB;