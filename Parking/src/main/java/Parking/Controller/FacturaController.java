package Parking.Controller;
import Parking.Models.Factura;
import Parking.Service.FacturaService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/factura")
public class FacturaController {
    
    @Autowired
    private FacturaService facturaservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Factura> agregar(@RequestBody Factura factura){
        Factura obj = facturaservice.save(factura);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Factura> editar(@RequestBody Factura factura){
    Factura obj = facturaservice.findById(factura.getId_factura());
    if(obj!=null){
        obj.setFecha_ingreso(factura.getFecha_ingreso());
        obj.setFecha_salida(factura.getFecha_salida());
        obj.setId_tarifa(factura.getId_tarifa());
        obj.setPlaca(factura.getPlaca());
        obj.setId_plaza(factura.getId_plaza());
        facturaservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Factura> eliminar(@PathVariable Integer id_factura){
    Factura obj = facturaservice.findById(id_factura);
    if(obj!=null)
    facturaservice.delete(id_factura);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Factura consultaPorId(@PathVariable Integer id_factura){
        return facturaservice.findById(id_factura);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Factura> consultarTodo(){
        return facturaservice.findAll();
    }
}