package Parking.Controller;
import Parking.Models.Tarifa;
import Parking.Service.TarifaService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/tarifa")
public class TarifaController {
    
    @Autowired
    private TarifaService tarifaservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Tarifa> agregar(@RequestBody Tarifa tarifa){
        Tarifa obj = tarifaservice.save(tarifa);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Tarifa> editar(@RequestBody Tarifa tarifa){
    Tarifa obj = tarifaservice.findById(tarifa.getId_tarifa());
    if(obj!=null){
        obj.setValor(tarifa.getValor());
        obj.setTipo(tarifa.getTipo());
        tarifaservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Tarifa> eliminar(@PathVariable Integer id_tarifa){
    Tarifa obj = tarifaservice.findById(id_tarifa);
    if(obj!=null)
    tarifaservice.delete(id_tarifa);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Tarifa consultaPorId(@PathVariable Integer id_tarifa){
        return tarifaservice.findById(id_tarifa);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Tarifa> consultarTodo(){
        return tarifaservice.findAll();
    }
}