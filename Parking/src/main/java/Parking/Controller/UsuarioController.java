package Parking.Controller;
import Parking.Models.Usuario;
import Parking.Service.UsuarioService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    private UsuarioService usuarioservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario){
        Usuario obj = usuarioservice.save(usuario);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario){
    Usuario obj = usuarioservice.findById(usuario.getId_usuario());
    if(obj!=null){
        obj.setNombre(usuario.getNombre());
        obj.setCorreo(usuario.getCorreo());
        obj.setContraseña(usuario.getContraseña());
        obj.setId_rol(usuario.getId_rol());
        obj.setId_persona(usuario.getId_persona());
        usuarioservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id_usuario){
    Usuario obj = usuarioservice.findById(id_usuario);
    if(obj!=null)
    usuarioservice.delete(id_usuario);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Usuario consultaPorId(@PathVariable Integer id_usuario){
        return usuarioservice.findById(id_usuario);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Usuario> consultarTodo(){
        return usuarioservice.findAll();
    }
}