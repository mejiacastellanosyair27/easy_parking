package Parking.Controller;
import Parking.Models.Tipo_vehiculo;
import Parking.Service.Tipo_vehiculoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/tipo_vehiculo")
public class Tipo_vehiculoController {
    
    @Autowired
    private Tipo_vehiculoService tipovehiculoservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Tipo_vehiculo> agregar(@RequestBody Tipo_vehiculo topo_vehiculo){
        Tipo_vehiculo obj = tipovehiculoservice.save(topo_vehiculo);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Tipo_vehiculo> editar(@RequestBody Tipo_vehiculo tipo_vehiculo){
    Tipo_vehiculo obj = tipovehiculoservice.findById(tipo_vehiculo.getId_tipovehiculo());
    if(obj!=null){
        obj.setTipo(tipo_vehiculo.getTipo());
        obj.setDescripcion(tipo_vehiculo.getDescripcion());
        tipovehiculoservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Tipo_vehiculo> eliminar(@PathVariable Integer id_tipovehiculo){
    Tipo_vehiculo obj = tipovehiculoservice.findById(id_tipovehiculo);
    if(obj!=null)
    tipovehiculoservice.delete(id_tipovehiculo);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Tipo_vehiculo consultaPorId(@PathVariable Integer id_tipovehiculo){
        return tipovehiculoservice.findById(id_tipovehiculo);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Tipo_vehiculo> consultarTodo(){
        return tipovehiculoservice.findAll();
    }
}