package Parking.Controller;
import Parking.Models.Rol_usuario;
import Parking.Service.Rol_usuarioService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/rol_usuario")
public class Rol_usuarioController {
    
    @Autowired
    private Rol_usuarioService rolservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Rol_usuario> agregar(@RequestBody Rol_usuario rol_usuario){
        Rol_usuario obj = rolservice.save(rol_usuario);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Rol_usuario> editar(@RequestBody Rol_usuario rol_usuario){
    Rol_usuario obj = rolservice.findById(rol_usuario.getId_rol());
    if(obj!=null){
        obj.setNombre_rol(rol_usuario.getNombre_rol());
        obj.setDescripcion(rol_usuario.getDescripcion());
        rolservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Rol_usuario> eliminar(@PathVariable Integer id_rol){
    Rol_usuario obj = rolservice.findById(id_rol);
    if(obj!=null)
    rolservice.delete(id_rol);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Rol_usuario consultaPorId(@PathVariable Integer id_rol){
        return rolservice.findById(id_rol);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Rol_usuario> consultarTodo(){
        return rolservice.findAll();
    }
}