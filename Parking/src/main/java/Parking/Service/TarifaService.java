package Parking.Service;

import Parking.Models.Tarifa;
import java.util.List;

public interface TarifaService {
    public Tarifa save (Tarifa tarifa);
    public void delete(Integer id_tarifa);
    public Tarifa findById(Integer id_tarifa);
    public List<Tarifa> findAll();
}