package Parking.Service;

import Parking.Models.Rol_usuario;
import java.util.List;

public interface Rol_usuarioService {
    public Rol_usuario save (Rol_usuario rol_usuario);
    public void delete(Integer id_rol);
    public Rol_usuario findById(Integer id_rol);
    public List<Rol_usuario> findAll();
}