package Parking.Service;

import Parking.Models.Plaza;
import java.util.List;

public interface PlazaService {
    public Plaza save (Plaza plaza);
    public void delete(String id_plaza);
    public Plaza findById(String id_plaza);
    public List<Plaza> findAll();
}