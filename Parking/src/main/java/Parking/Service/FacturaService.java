package Parking.Service;

import Parking.Models.Factura;
import java.util.List;

public interface FacturaService {
    public Factura save (Factura factura);
    public void delete(Integer id_factura);
    public Factura findById(Integer id_factura);
    public List<Factura> findAll();
}