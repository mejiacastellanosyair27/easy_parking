package Parking.Service;

import Parking.Models.Persona;
import java.util.List;

public interface PersonaService {
    public Persona save (Persona persona);
    public void delete(Integer id_persona);
    public Persona findById(Integer id_persona);
    public List<Persona> findAll();
}