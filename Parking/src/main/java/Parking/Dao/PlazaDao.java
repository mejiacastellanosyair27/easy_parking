package Parking.Dao;

import Parking.Models.Plaza;
import org.springframework.data.repository.CrudRepository;

public interface PlazaDao extends CrudRepository<Plaza,String>{}