package Parking.Dao;

import Parking.Models.Rol_usuario;
import org.springframework.data.repository.CrudRepository;

public interface Rol_usuarioDao extends CrudRepository<Rol_usuario,Integer>{}