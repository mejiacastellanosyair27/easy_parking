package Parking.Dao;

import Parking.Models.Tipo_vehiculo;
import org.springframework.data.repository.CrudRepository;

public interface Tipo_vehiculoDao extends CrudRepository<Tipo_vehiculo,Integer>{}