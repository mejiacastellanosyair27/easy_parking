package Parking.Dao;

import Parking.Models.Vehiculo;
import org.springframework.data.repository.CrudRepository;

public interface VehiculoDao extends CrudRepository<Vehiculo,String>{}