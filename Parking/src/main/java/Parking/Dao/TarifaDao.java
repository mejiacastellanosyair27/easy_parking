package Parking.Dao;

import Parking.Models.Tarifa;
import org.springframework.data.repository.CrudRepository;

public interface TarifaDao extends CrudRepository<Tarifa,Integer>{}