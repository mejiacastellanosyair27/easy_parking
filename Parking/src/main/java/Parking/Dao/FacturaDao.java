package Parking.Dao;

import Parking.Models.Factura;
import org.springframework.data.repository.CrudRepository;

public interface FacturaDao extends CrudRepository<Factura,Integer>{}