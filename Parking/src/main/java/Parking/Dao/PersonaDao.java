package Parking.Dao;

import Parking.Models.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaDao extends CrudRepository<Persona,Integer>{}