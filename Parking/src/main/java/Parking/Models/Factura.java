package Parking.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="factura")
public class Factura implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_factura")
    private int id_factura;

    @Column(name="fecha_ingreso")
    private String fecha_ingreso;

    @Column(name="fecha_salida")
    private String fecha_salida;

    @ManyToOne
    @JoinColumn(name="id_tarifa")
    private Tarifa id_tarifa;

    @ManyToOne
    @JoinColumn(name="placa_vehiculo")
    private Vehiculo placa;

    @ManyToOne
    @JoinColumn(name="id_plaza")
    private Plaza id_plaza;

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public void setId_tarifa(Tarifa id_tarifa) {
        this.id_tarifa = id_tarifa;
    }

    public void setPlaca(Vehiculo placa) {
        this.placa = placa;
    }

    public void setId_plaza(Plaza id_plaza) {
        this.id_plaza = id_plaza;
    }

    public int getId_factura() {
        return id_factura;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public Tarifa getId_tarifa() {
        return id_tarifa;
    }

    public Vehiculo getPlaca() {
        return placa;
    }

    public Plaza getId_plaza() {
        return id_plaza;
    }
}