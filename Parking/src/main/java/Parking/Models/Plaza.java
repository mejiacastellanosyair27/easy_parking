package Parking.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="plaza")
public class Plaza implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_plaza")
    private String id_plaza;

    @Column(name="tipo")
    private String tipo;

    @Column(name="disponibilidad")
    private boolean disponibilidad;

    public void setId_plaza(String id_plaza) {
        this.id_plaza = id_plaza;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getId_plaza() {
        return id_plaza;
    }

    public String getTipo() {
        return tipo;
    }

    public boolean getDisponibilidad() {
        return disponibilidad;
    }
}