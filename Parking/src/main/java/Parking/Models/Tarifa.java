package Parking.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="tarifa")
public class Tarifa implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_tarifa")
    private int id_tarifa;

    @Column(name="valor")
    private double valor;

    @Column(name="tipo")
    private String tipo;

    public void setId_tarifa(int id_tarifa) {
        this.id_tarifa = id_tarifa;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getId_tarifa() {
        return id_tarifa;
    }

    public double getValor() {
        return valor;
    }

    public String getTipo() {
        return tipo;
    }
}