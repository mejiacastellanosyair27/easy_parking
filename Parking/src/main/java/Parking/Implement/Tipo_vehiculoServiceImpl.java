package Parking.Implement;

import Parking.Dao.Tipo_vehiculoDao;
import Parking.Models.Tipo_vehiculo;
import Parking.Service.Tipo_vehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Tipo_vehiculoServiceImpl implements Tipo_vehiculoService{

    @Autowired
    private Tipo_vehiculoDao tipo_vehiculoDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Tipo_vehiculo save(Tipo_vehiculo tipo_vehiculo){
        return tipo_vehiculoDao.save(tipo_vehiculo);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id_tipovehiculo){
        tipo_vehiculoDao.deleteById(id_tipovehiculo);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Tipo_vehiculo findById(Integer id_tipovehiculo){
        return tipo_vehiculoDao.findById(id_tipovehiculo).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Tipo_vehiculo> findAll(){
        return (List<Tipo_vehiculo>) tipo_vehiculoDao.findAll();
    }
}