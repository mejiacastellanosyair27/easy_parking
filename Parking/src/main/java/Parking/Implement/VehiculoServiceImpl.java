package Parking.Implement;

import Parking.Dao.VehiculoDao;
import Parking.Models.Vehiculo;
import Parking.Service.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VehiculoServiceImpl implements VehiculoService{

    @Autowired
    private VehiculoDao vehiculoDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Vehiculo save(Vehiculo vehiculo){
        return vehiculoDao.save(vehiculo);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete(String placa){
        vehiculoDao.deleteById(placa);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Vehiculo findById(String placa){
        return vehiculoDao.findById(placa).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Vehiculo> findAll(){
        return (List<Vehiculo>) vehiculoDao.findAll();
    }
}