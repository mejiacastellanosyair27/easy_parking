package Parking.Implement;

import Parking.Dao.PersonaDao;
import Parking.Models.Persona;
import Parking.Service.PersonaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonasServiceImpl implements PersonaService{
    
    @Autowired
    private PersonaDao personasDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Persona save(Persona persona){
        return personasDao.save(persona);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id_persona){
        personasDao.deleteById(id_persona);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Persona findById(Integer id_persona){
        return personasDao.findById(id_persona).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Persona> findAll(){
        return (List<Persona>) personasDao.findAll();
    }
}