package Parking.Implement;

import Parking.Dao.TarifaDao;
import Parking.Models.Tarifa;
import Parking.Service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TarifaServiceImpl implements TarifaService{
    
    @Autowired
    private TarifaDao tarifaDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Tarifa save(Tarifa tarifa){
        return tarifaDao.save(tarifa);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id_tarifa){
        tarifaDao.deleteById(id_tarifa);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Tarifa findById(Integer id_tarifa){
        return tarifaDao.findById(id_tarifa).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Tarifa> findAll(){
        return (List<Tarifa>) tarifaDao.findAll();
    }
}