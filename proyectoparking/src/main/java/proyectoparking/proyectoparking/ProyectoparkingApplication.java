package proyectoparking.proyectoparking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoparkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoparkingApplication.class, args);
	}

}
