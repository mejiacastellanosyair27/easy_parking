package proyectoparking.proyectoparking.Controller;
import proyectoparking.proyectoparking.Models.Persona;
import proyectoparking.proyectoparking.Service.PersonaService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/persona")
public class PersonaController {
    
    @Autowired
    private PersonaService personaservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Persona> agregar(@RequestBody Persona persona){
        Persona obj = personaservice.save(persona);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Persona> editar(@RequestBody Persona persona){
    Persona obj = personaservice.findById(persona.getId_persona());
    if(obj!=null){
        obj.setNombre(persona.getNombre());
        obj.setApellido(persona.getApellido());
        obj.setTelefono(persona.getTelefono());
        obj.setDireccion(persona.getDireccion());
        personaservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Persona> eliminar(@PathVariable Integer id_persona){
    Persona obj = personaservice.findById(id_persona);
    if(obj!=null)
    personaservice.delete(id_persona);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Persona consultaPorId(@PathVariable Integer id_persona){
        return personaservice.findById(id_persona);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Persona> consultarTodo(){
        return personaservice.findAll();
    }
}