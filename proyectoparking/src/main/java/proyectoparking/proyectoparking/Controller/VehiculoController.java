package proyectoparking.proyectoparking.Controller;
import proyectoparking.proyectoparking.Models.Vehiculo;
import proyectoparking.proyectoparking.Service.VehiculoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/vehiculo")
public class VehiculoController {
    
    @Autowired
    private VehiculoService vehiculoservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo){
        Vehiculo obj = vehiculoservice.save(vehiculo);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculo){
    Vehiculo obj = vehiculoservice.findById(vehiculo.getPlaca());
    if(obj!=null){
        obj.setId_tipovehiculo(vehiculo.getId_tipovehiculo());
        obj.setId_persona(vehiculo.getId_persona());
        obj.setCaracteristicas(vehiculo.getCaracteristicas());
        vehiculoservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Vehiculo> eliminar(@PathVariable String placa){
    Vehiculo obj = vehiculoservice.findById(placa);
    if(obj!=null)
    vehiculoservice.delete(placa);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Vehiculo consultaPorId(@PathVariable String placa){
        return vehiculoservice.findById(placa);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Vehiculo> consultarTodo(){
        return vehiculoservice.findAll();
    }
}