package proyectoparking.proyectoparking.Controller;
import proyectoparking.proyectoparking.Models.Plaza;
import proyectoparking.proyectoparking.Service.PlazaService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class PlazaController {
    
    @Autowired
    private PlazaService plazaservice;

    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Plaza> agregar(@RequestBody Plaza plaza){
        Plaza obj = plazaservice.save(plaza);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    //ACTUALIZAR - EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Plaza> editar(@RequestBody Plaza plaza){
    Plaza obj = plazaservice.findById(plaza.getId_plaza());
    if(obj!=null){
        obj.setTipo(plaza.getTipo());
        obj.setDisponibilidad(plaza.getDisponibilidad());
        plazaservice.save(obj);
    }
    else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Plaza> eliminar(@PathVariable String id_plaza){
    Plaza obj = plazaservice.findById(id_plaza);
    if(obj!=null)
    plazaservice.delete(id_plaza);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR ID
    @GetMapping("/list/{id}")
    public Plaza consultaPorId(@PathVariable String id_plaza){
        return plazaservice.findById(id_plaza);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Plaza> consultarTodo(){
        return plazaservice.findAll();
    }
}