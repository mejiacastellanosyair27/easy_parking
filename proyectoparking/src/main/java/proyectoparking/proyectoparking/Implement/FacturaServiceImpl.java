package proyectoparking.proyectoparking.Implement;

import proyectoparking.proyectoparking.Dao.FacturaDao;
import proyectoparking.proyectoparking.Models.Factura;
import proyectoparking.proyectoparking.Service.FacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FacturaServiceImpl implements FacturaService{
    
    @Autowired
    private FacturaDao facturaDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Factura save(Factura factura){
        return facturaDao.save(factura);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id_factura){
        facturaDao.deleteById(id_factura);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Factura findById(Integer id_factura){
        return facturaDao.findById(id_factura).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Factura> findAll(){
        return (List<Factura>) facturaDao.findAll();
    }
}