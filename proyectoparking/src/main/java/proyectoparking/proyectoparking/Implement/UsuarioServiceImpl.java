package proyectoparking.proyectoparking.Implement;

import proyectoparking.proyectoparking.Dao.UsuarioDao;
import proyectoparking.proyectoparking.Models.Usuario;
import proyectoparking.proyectoparking.Service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioServiceImpl implements UsuarioService{

    @Autowired
    private UsuarioDao usuarioDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Usuario save(Usuario usuario){
        return usuarioDao.save(usuario);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id_usuario){
        usuarioDao.deleteById(id_usuario);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Usuario findById(Integer id_usuario){
        return usuarioDao.findById(id_usuario).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Usuario> findAll(){
        return (List<Usuario>) usuarioDao.findAll();
    }
}