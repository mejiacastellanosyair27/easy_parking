package proyectoparking.proyectoparking.Implement;

import proyectoparking.proyectoparking.Dao.Rol_usuarioDao;
import proyectoparking.proyectoparking.Models.Rol_usuario;
import proyectoparking.proyectoparking.Service.Rol_usuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Rol_usuarioServiceImpl implements Rol_usuarioService{
    
    @Autowired
    private Rol_usuarioDao rol_usuarioDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Rol_usuario save(Rol_usuario rol_usuario){
        return rol_usuarioDao.save(rol_usuario);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id_rol){
        rol_usuarioDao.deleteById(id_rol);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Rol_usuario findById(Integer id_rol){
        return rol_usuarioDao.findById(id_rol).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Rol_usuario> findAll(){
        return (List<Rol_usuario>) rol_usuarioDao.findAll();
    }
}