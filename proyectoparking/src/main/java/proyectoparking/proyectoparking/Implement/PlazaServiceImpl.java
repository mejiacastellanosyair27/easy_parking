package proyectoparking.proyectoparking.Implement;

import proyectoparking.proyectoparking.Dao.PlazaDao;
import proyectoparking.proyectoparking.Models.Plaza;
import proyectoparking.proyectoparking.Service.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlazaServiceImpl implements PlazaService{
    
    @Autowired
    private PlazaDao plazaDao;

    //INSERTAR
    @Override
    @Transactional(readOnly=false)
    public Plaza save(Plaza plaza){
        return plazaDao.save(plaza);
    }

    //ELIMINAR
    @Override
    @Transactional(readOnly=false)
    public void delete (String id_plaza){
        plazaDao.deleteById(id_plaza);
    }

    //CONSULTAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Plaza findById(String id_plaza){
        return plazaDao.findById(id_plaza).orElse(null);
    }

    //CONSULTAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Plaza> findAll(){
        return (List<Plaza>) plazaDao.findAll();
    }
}