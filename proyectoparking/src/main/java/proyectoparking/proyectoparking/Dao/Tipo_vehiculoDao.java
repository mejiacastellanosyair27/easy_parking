package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Tipo_vehiculo;
import org.springframework.data.repository.CrudRepository;

public interface Tipo_vehiculoDao extends CrudRepository<Tipo_vehiculo,Integer>{}