package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Rol_usuario;
import org.springframework.data.repository.CrudRepository;

public interface Rol_usuarioDao extends CrudRepository<Rol_usuario,Integer>{}