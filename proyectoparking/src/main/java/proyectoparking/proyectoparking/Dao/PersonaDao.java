package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaDao extends CrudRepository<Persona,Integer>{}