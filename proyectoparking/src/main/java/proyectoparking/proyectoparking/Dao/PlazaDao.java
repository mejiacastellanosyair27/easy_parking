package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Plaza;
import org.springframework.data.repository.CrudRepository;

public interface PlazaDao extends CrudRepository<Plaza,String>{}