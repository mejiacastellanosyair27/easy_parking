package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Vehiculo;
import org.springframework.data.repository.CrudRepository;

public interface VehiculoDao extends CrudRepository<Vehiculo,String>{}