package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Factura;
import org.springframework.data.repository.CrudRepository;

public interface FacturaDao extends CrudRepository<Factura,Integer>{}