package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Tarifa;
import org.springframework.data.repository.CrudRepository;

public interface TarifaDao extends CrudRepository<Tarifa,Integer>{}