package proyectoparking.proyectoparking.Dao;

import proyectoparking.proyectoparking.Models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioDao extends CrudRepository<Usuario,Integer> {}