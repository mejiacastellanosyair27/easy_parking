package proyectoparking.proyectoparking.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="vehiculo")
public class Vehiculo implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="placa")
    private String placa;

    @ManyToOne
    @JoinColumn(name="id_tipovehiculo")
    private Tipo_vehiculo id_tipovehiculo;

    @ManyToOne
    @JoinColumn(name="id_propietario")
    private Persona id_persona;

    @Column(name="caracteristicas")
    private String caracteristicas;

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setId_tipovehiculo(Tipo_vehiculo id_tipovehiculo) {
        this.id_tipovehiculo = id_tipovehiculo;
    }

    public void setId_persona(Persona id_persona) {
        this.id_persona = id_persona;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getPlaca() {
        return placa;
    }

    public Tipo_vehiculo getId_tipovehiculo() {
        return id_tipovehiculo;
    }

    public Persona getId_persona() {
        return id_persona;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }
}