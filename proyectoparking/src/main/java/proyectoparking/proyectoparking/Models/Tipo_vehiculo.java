package proyectoparking.proyectoparking.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="tipo_vehiculo")
public class Tipo_vehiculo implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_tipovehiculo")
    private int id_tipovehiculo;

    @Column(name="tipo")
    private String tipo;

    @Column(name="descripcion")
    private String descripcion;

    public void setId(int id_tipovehiculo) {
        this.id_tipovehiculo = id_tipovehiculo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_tipovehiculo() {
        return this.id_tipovehiculo;
    }

    public String getTipo() {
        return this.tipo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }
}