package proyectoparking.proyectoparking.Service;

import proyectoparking.proyectoparking.Models.Vehiculo;
import java.util.List;

public interface VehiculoService {
    public Vehiculo save (Vehiculo vehiculo);
    public void delete(String placa);
    public Vehiculo findById(String placa);
    public List<Vehiculo> findAll();
}