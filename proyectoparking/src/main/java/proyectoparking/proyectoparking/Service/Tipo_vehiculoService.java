package proyectoparking.proyectoparking.Service;

import proyectoparking.proyectoparking.Models.Tipo_vehiculo;
import java.util.List;

public interface Tipo_vehiculoService {
    public Tipo_vehiculo save (Tipo_vehiculo tipo_vehiculo);
    public void delete(Integer id_tipovehiculo);
    public Tipo_vehiculo findById(Integer id_tipovehiculo);
    public List<Tipo_vehiculo> findAll();
}