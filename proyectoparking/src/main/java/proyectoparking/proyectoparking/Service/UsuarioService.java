package proyectoparking.proyectoparking.Service;

import proyectoparking.proyectoparking.Models.Usuario;
import java.util.List;

public interface UsuarioService{
    public Usuario save (Usuario usuario);
    public void delete(Integer id_usuario);
    public Usuario findById(Integer id_usuario);
    public List<Usuario> findAll();
}