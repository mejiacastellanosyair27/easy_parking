create DATABASE parking;
use parking;
create table usuario (
	id_usuario int not null auto_increment unique,
	nombre varchar(30),
    correo varchar(64),
    contraseña varchar(16),
    id_rol int,
    id_persona int,
    primary key (id_usuario),
    FOREIGN KEY (id_rol) REFERENCES rol_usuario(id_rol),
	FOREIGN KEY (id_persona) REFERENCES persona(id_persona)
)engine = InnoDB;

create table persona(
	id_persona int not null,
    nombre varchar(30),
    apellido varchar(30),
    telefono varchar(15),
    direccion varchar(40),
    primary key (id_persona)
)engine = InnoDB;

create table rol_usuario(
	id_rol int not null auto_increment,
    nombre_rol varchar(15) unique,
    descripcion varchar (50),
    primary key (id_rol)
)engine = InnoDB;

create table tipo_vehiculo(
	id_tipovehiculo int not null auto_increment,
    tipo varchar(10),
    descripcion varchar(50),
    primary key (id_tipovehiculo)
)engine = InnoDB;

create table vehiculo(
	placa varchar(6),
    id_tipovehiculo int,
    id_propietario int,
    caracteristicas varchar(100),
    primary key (placa),
    FOREIGN KEY (id_tipovehiculo) REFERENCES tipo_vehiculo(id_tipovehiculo),
    FOREIGN KEY (id_propietario) REFERENCES persona(id_persona)
)engine = InnoDB;

create table plaza(
	id_plaza varchar (4) not null unique,
    tipo varchar(20),
    disponibilidad boolean default true,
    primary key(id_plaza)
)engine = InnoDB;

create table tarifa(
	id_tarifa int not null auto_increment,
    valor double,
    tipo varchar(20),
    primary key(id_tarifa)
)engine = InnoDB;

create table factura(
	id_factura int not null auto_increment,
    fecha_ingreso datetime,
    fecha_salida datetime,
    id_tarifa int,
    placa_vehiculo varchar(6),
    id_plaza varchar(4),
    primary key(id_factura),
    foreign key(id_tarifa) references tarifa(id_tarifa),
    foreign key(placa_vehiculo) references vehiculo(placa),
    foreign key(id_plaza) references plaza(id_plaza)
)engine = InnoDB;